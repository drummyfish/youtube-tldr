# YouTube tl;dr

This is a simple experimental website and database for short YouTube video
summaries.  It should tell you the main point of a video, to save you time.
Accessible at:

TODO

**Contributions of tl;drs are welcome!** By contributing you agree to
releasing your contribution under CC0 1.0, public domain (you waive all
copyright).
